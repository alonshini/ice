﻿#nullable enable
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailPointView : MonoBehaviour
{
    //private MeshRenderer m_renderer;
    
    [SerializeField] LineRenderer m_lineRenderer;
    [SerializeField] SpriteRenderer m_mainImage;
    private Gradient m_defaultGradiant;

    public void SetDefaultGradiant(Gradient gradient)
    {
        m_defaultGradiant = gradient;
    }
    public void SetData(bool isOn, Vector3 pos, Vector3 prevPoint, Gradient? overrideColor=null)
    {
        if (!isOn)
        {
            gameObject.SetActive(false);
            return;
        }
        
        gameObject.SetActive(true);
        transform.position = pos;
      
        
        m_lineRenderer.SetPosition(0,new Vector3(pos.x,pos.y,-1));
        m_lineRenderer.SetPosition(1,new Vector3(prevPoint.x,prevPoint.y,-1));

        if (overrideColor != null)
        {
            m_lineRenderer.colorGradient = overrideColor;
            m_mainImage.enabled = false;
            
            //also make the point not at the pos - but 0.7 between prev and pos
            Vector3 midPoint = Vector3.Lerp(prevPoint, pos, 0.7f);
            m_lineRenderer.SetPosition(0,new Vector3(midPoint.x,midPoint.y,-1));

            transform.position = midPoint;
        }
    }

    

    public void Reset()
    {
        gameObject.SetActive(false);
        m_mainImage.enabled = true;
        m_lineRenderer.colorGradient = m_defaultGradiant;
    }
}
