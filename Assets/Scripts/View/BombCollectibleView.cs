using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombCollectibleView : CollectibleView
{

    [SerializeField] private GameObject m_explositionParticles;
    public float m_bombSize = 3f;
   
    public override void PlayCollideAnimation()
    {
        m_mainCollider.enabled = false;
        m_explositionParticles.SetActive(true);
        m_mainImage.enabled = false;
    }
}
