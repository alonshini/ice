using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class InstructionsView : MonoBehaviour
{
    [SerializeField] private TMP_Text m_instructions;
    [SerializeField] private CanvasGroup m_instructionsCanvas;
    private Action m_showInstructionsDoneCallback;

    private void Awake()
    {
        m_instructionsCanvas.alpha = 0;
    }

    public void ShowInstructions(string text = null, float autoHide = 0, Action ShowDone=null)
    {
        m_showInstructionsDoneCallback = ShowDone;

        if (text != null)
            m_instructions.text = text;

        m_instructionsCanvas.DOFade(1, 0.25f);

        if (autoHide > 0)
            Invoke("HideInstructions", autoHide);
    }

    public void HideInstructions()
    {
        m_instructionsCanvas.DOFade(0, 0.25f);
        m_showInstructionsDoneCallback?.Invoke();
    }
}
