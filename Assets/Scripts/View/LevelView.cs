using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelView : MonoBehaviour
{
    [SerializeField] private int m_numShots;
    [SerializeField] private int m_levelForceMulti = 1000;
    [SerializeField] private Transform m_holder;
    private GameObject m_dispenser;
    private float m_ballSize;
    private int m_numCollectibles;
    
    public Transform Holder => m_holder;

    public int NumShots => m_numShots;

   

    public GameObject Dispenser => m_dispenser;
    
    public void SetDispenserObject(GameObject dispenser)
    {
        m_dispenser = dispenser;
        m_ballSize = m_dispenser.transform.localScale.x;
    }
    
    public float BallSize => m_ballSize;

    public int NumCollectibles
    {
        get => m_numCollectibles;
        set => m_numCollectibles = value;
    }

    public int LevelForceMulti => m_levelForceMulti;
}
