using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraView : MonoBehaviour
{
    
    // Start is called before the first frame update
    public void SetCamera(float sizeX)
    {
        float orthSize = sizeX * Screen.height / Screen.width * 0.5f;
        Camera.main.orthographicSize = orthSize;
    }

}
