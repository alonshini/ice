using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CollectibleView : MonoBehaviour
{
    public Vector3 m_baseScale = new Vector3();
    public const float BIG_SCALE_FACTOR = 3f;

    public const float COLLECT_ANIM_TIME = 2f;
    public const float APPEAR_ANIM_TIME = .65f;

    
    public enum CollectibleType
    {
        Fish,
        Bomb
    }

    public CollectibleType m_collectibleType;
    [SerializeField] protected SpriteRenderer m_mainImage;
    [SerializeField] protected Collider2D m_mainCollider;
    [SerializeField] private GameObject m_waterSplash;

    public void Init(Transform parent)
    {
        transform.SetParent(parent);
        m_baseScale = m_mainImage.transform.localScale;
        m_mainImage.DOFade(0, 0.1f);
    }
    
    public virtual void PlayCollideAnimation()
    {
        m_mainCollider.enabled = false;
        //move to be over ice
        m_mainImage.sortingOrder = 50;
        ActivateSplash();
        m_mainImage.transform.DOPunchScale(m_baseScale*BIG_SCALE_FACTOR, COLLECT_ANIM_TIME);
        m_mainImage.DOFade(0, 1).SetDelay(COLLECT_ANIM_TIME/2f);

    }

    public virtual void AppearInWater()
    {
        m_mainImage.transform.localScale = m_baseScale*BIG_SCALE_FACTOR;
        m_mainImage.transform.DOScale(m_baseScale, APPEAR_ANIM_TIME);
        m_mainImage.DOFade(1, APPEAR_ANIM_TIME/2f).OnComplete(ActivateSplash);
    }

    private void ActivateSplash()
    {
        m_waterSplash.SetActive(false);
        m_waterSplash.SetActive(true);
        
        //spalsh SFX
    }
}
