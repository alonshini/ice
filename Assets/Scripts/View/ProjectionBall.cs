using System;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class ProjectionBall : MonoBehaviour
{
    
    [SerializeField] private Rigidbody2D m_rb;
    private bool m_colliding=false;

    public bool Colliding
    {
        get => m_colliding;
        set => m_colliding = value;
    }


    public void Init(float scale,Vector3 position, Vector2 velocity)
    {
        transform.localScale = new Vector3(scale, scale, 1);
        transform.position = position;
        Colliding = false;
        m_rb.isKinematic = false;
        m_rb.AddForce(velocity, ForceMode2D.Impulse);
     }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.CompareTag("ProjectionBall"))
            return;
        
        Colliding = true;
    }

}