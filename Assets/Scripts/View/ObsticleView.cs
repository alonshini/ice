using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsticleView : MonoBehaviour
{
    enum ObsticleType
    {
        Rect,
        Circ
    }

    [SerializeField] ObsticleType m_obsticleType;

    [SerializeField] SpriteRenderer m_mainImage;
    [SerializeField] Collider2D m_collider;

    
}
