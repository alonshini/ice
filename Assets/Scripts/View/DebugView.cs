using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DebugView : MonoBehaviour
{
    [SerializeField] private TMP_Text m_levelIndex;

    public void ShowHide(bool show)
    {
        gameObject.SetActive(show);
    }

    public void UpdateLevelIndex(string levelText)
    {
        m_levelIndex.text = levelText;
    }
}
