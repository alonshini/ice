using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class LevelSummaryView : MonoBehaviour
{
    [SerializeField] private TMP_Text m_mainText;
    [SerializeField] private CanvasGroup m_mainCanvas;

    private void Awake()
    {
        m_mainCanvas.alpha = 0;
    }

    public void ShowLevelSummary(bool success)
    {
        m_mainCanvas.alpha = 0;
        gameObject.SetActive(true);
        m_mainText.text = success ? "Success" : "Failed";
        m_mainCanvas.DOFade(1, 1);
    }

    public void HideLevelSummary()
    {
        m_mainCanvas.DOFade(0, 1).OnComplete(() =>
        {
            gameObject.SetActive(false);
        });
    }

}
