﻿
using UnityEditor;
using UnityEngine;


public class BallView : MonoBehaviour
{
    private const float BALL_FORCE_MULTI = 3f;
    
    Rigidbody2D m_rigidbody;
    CircleCollider2D m_collider2D;
    bool m_active = false;
    private Vector2 m_prevPos = new Vector2();

    private void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();
        m_collider2D = GetComponent<CircleCollider2D>();
    }

    public void SetMe(Vector3 scale)
    {
        transform.localScale = scale;
    }


    public void ShotMe(Vector2 vector2)
    {
        //m_rigidbody.drag = 0;
        m_active = true;
        m_prevPos = transform.position;
        m_collider2D.enabled = true;
        m_rigidbody.isKinematic = false;
        AddForce(vector2);
        
    }

    public void HitByBomb()
    {
        //cancel slow down
        //m_rigidbody.drag = 0;
        
        Vector3 currDirection = Quaternion.Euler(0, 0, Random.Range(0,360)) * Vector3.right;
        currDirection.Normalize();
        m_rigidbody.velocity = Vector2.zero;
        AddForce(currDirection * GameController.Instance.GetCurrLevelForceMulti());
    }

    private void AddForce(Vector2 force)
    {
        m_rigidbody.AddForce(force,ForceMode2D.Impulse);
    }
    
    private void LateUpdate()
    {
        if (m_active)
        {
            Vector2 direction = new Vector2(transform.position.x - m_prevPos.x, transform.position.y - m_prevPos.y);

            if (direction.magnitude > 2.5f)
            {
                //direction.Normalize();

                Vector2 perp = Vector2.Perpendicular(direction);

                GameController.Instance.ReportBallPosition(m_prevPos, transform.position, perp);
                m_prevPos = transform.position;
            }

            if (GameController.Instance.CheckForStop())
            {
                //Debug.Log("rigid vel: "+m_rigidbody.velocity.magnitude);
                if(m_rigidbody.velocity.magnitude < 2f)
                    GameController.Instance.BallStopped(transform.position);
            }
            
        }
    }

    public void SlowDown()
    {
        m_rigidbody.drag = 5f;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Collectibles"))
            GameController.Instance.BallHitTrigger(other);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Colliders"))
            GameController.Instance.BallHitCollider(transform.position);
    }
}