using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DispenserManager : MonoBehaviour
{
    [SerializeField] private SimulationManager m_siumationManager;
    [SerializeField] private Transform m_trailPointsHolder;

    private Transform m_arrow;
    private GameObject m_dispenserObject;
    private float m_currAngle;
    private LevelView m_currLevel;
    private int m_currPredictionPointIndex = 0;
    private Vector3 m_prevPoint;
    private Vector2 m_currShot = new Vector2();
    private int m_maxIndex = 0;
    private List<TrailPointView> m_trailPoints = new List<TrailPointView>();
    private Gradient m_fadeGradiant;
    private Gradient m_fullGradiant;
    private Color m_baseGradiantColor = new Color(1, 0.5f, 0);
    private bool m_showPrediction = false;
    private bool m_angleAdjusted = false;
    
    public Vector2 CurrShot => m_currShot;

    public GameObject DispenserObject => m_dispenserObject;

    public Gradient FullGradiant => m_fullGradiant;

    

    public void InitSimulation(Transform baseColliders,int numIterations)
    {
        
        BuildGradiantMaterials();
        m_siumationManager.CreatePhysicsScene(baseColliders,numIterations);

        foreach (Transform point in m_trailPointsHolder)
        {
            TrailPointView pointView=point.GetComponent<TrailPointView>();
            
            m_trailPoints.Add(pointView);
            pointView.SetDefaultGradiant(m_fullGradiant);
        }

        HideAllTrailPoints();
    }

    private void BuildGradiantMaterials()
    {
        //build gradiant material
        GradientColorKey[] colorKey;
        GradientAlphaKey[] alphaKey;
        GradientAlphaKey[] alphaKey2;
        m_fadeGradiant = new Gradient();
        m_fullGradiant = new Gradient();
        
        // Populate the color keys at the relative time 0 and 1 (0 and 100%)
        colorKey = new GradientColorKey[2];
        colorKey[0].color = m_baseGradiantColor;
        colorKey[0].time = 0.0f;
        colorKey[1].color = m_baseGradiantColor;
        colorKey[1].time = 1.0f;

        // Populate the alpha  keys at relative time 0 and 1  (0 and 100%)
        alphaKey = new GradientAlphaKey[3];
        alphaKey[0].alpha = 0.0f;
        alphaKey[0].time = 0.5f;
        alphaKey[1].alpha = 1.0f;
        alphaKey[1].time = 0.75f;
        alphaKey[2].alpha = 1.0f;
        alphaKey[2].time = 1.0f;

        alphaKey2 = new GradientAlphaKey[2];
        alphaKey2[0].alpha = 1.0f;
        alphaKey2[0].time = 0.0f;
        alphaKey2[1].alpha = 1.0f;
        alphaKey2[1].time = 1.0f;
        
        FullGradiant.SetKeys(colorKey,alphaKey2);
        m_fadeGradiant.SetKeys(colorKey, alphaKey);
    }

    
    public void SetDispenserObject(LevelView currLevel, LevelBuilder levelBuilder)
    {
        m_currLevel = currLevel;
        m_dispenserObject = currLevel.Dispenser;
        m_arrow = DispenserObject.transform.GetChild(1);
        //hide the dispenser indication - 
        DispenserObject.transform.GetChild(0).gameObject.SetActive(false);
        //hide the dispenser arrow
        DispenserObject.transform.GetChild(1).gameObject.SetActive(false);
        //build dispenser parts
        //add line renderer for the simulation

        m_currAngle = DispenserObject.transform.localEulerAngles.z;

        DispenserObject.transform.localEulerAngles = new Vector3();
        
        //init projection with level data
        m_siumationManager.killAllObstacles();
        m_siumationManager.copyAllObstacles(levelBuilder.Obsticles);
        m_siumationManager.SetPredictionCallBack(SimulationBallHitObsticle);

        ShowHidePrediction(false);

    }

    public void ShowHidePrediction(bool show)
    {
        m_showPrediction = show;
        
        if(!m_showPrediction)
            HideAllTrailPoints();
    }
    
    public void ResetDispenserObjectPosition(Vector3 newPosition)
    {
        DispenserObject.transform.position = newPosition;
        
        //also change the aim angle to be to screen center
        
        //in some cases - after the ball stopped - its almost touching a wall and the
        //prediction is into the wall - 
        
        float angle = Vector2.SignedAngle(Vector2.up, newPosition) + 540;

        m_currAngle = angle%360;
           
        AdjustAngle(Vector2.zero);
           
    }

    private void Update()
    {
        m_angleAdjusted = false;
    }

    private void LateUpdate()
    {
        if(m_showPrediction && !m_angleAdjusted)
            AdjustAngle(Vector2.zero);
    }

    private void SimulationBallHitObsticle(List<Vector2> position)
    {

        for (int i = 0; i < position.Count; i++)
        {
            m_trailPoints[i].SetData(true,position[i],m_prevPoint, position.Count-1==i?m_fadeGradiant:null);
            m_prevPoint = position[i];
        }
        
        //m_trailPoints[m_currPredictionPointIndex].SetData(true,position,m_prevPoint,
        //    m_currPredictionPointIndex==m_maxIndex?m_fadeGradiant:null);
        
        //m_trailPoints[m_currPredictionPointIndex].SetData(true,position,m_prevPoint, null);
        
        m_currPredictionPointIndex++;
        
        //m_prevPoint = position;
    }
    
    private void Predict()
    {
        
        m_arrow.localEulerAngles = new Vector3(0, 0, m_currAngle);
        
        Vector3 currDirection = Quaternion.Euler(0, 0, m_currAngle+90) * Vector3.right;
        currDirection.Normalize();
        m_currShot = currDirection * GameController.Instance.GetCurrLevelForceMulti();
        
        //Debug.Log("Angle: " + m_currAngle);
        
        //send predict
        m_siumationManager.Predict(m_dispenserObject.transform.localScale.x,m_dispenserObject.transform.position,CurrShot);
    }

    private void HideAllTrailPoints()
    {
        m_maxIndex = 0;
        foreach (TrailPointView point in m_trailPoints)
            point.Reset();
    }
    
    public void AdjustAngle(Vector2 angleDelta)
    {
        m_angleAdjusted = true;
        
        float dX = angleDelta.x / 1000f;
        
        //Debug.Log("DX: " + dX);
        
        //check if the arrow pointing left or right
        //0-180
        //-180 - 0

        //if (m_dispenserObject.transform.position.y > 0)
        //    dX *= -1;
        
        m_currAngle = (m_currAngle + 360) % 360;

        if (m_currAngle > 0 && m_currAngle < 180)
            //left side
            dX += angleDelta.y / 1000f;
        else
            dX -= angleDelta.y / 1000f;
        
        m_currAngle -= dX;
        //Debug.Log("Angle:"+ m_currAngle);
        //clear the prev prediction and reset counters
        m_prevPoint = m_dispenserObject.transform.position;
        m_currPredictionPointIndex = 0;
        HideAllTrailPoints();
        Predict();
    }

  
    
}
