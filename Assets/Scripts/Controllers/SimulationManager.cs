using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SimulationManager : MonoBehaviour
{
    int m_maxIterations;
    Scene m_predictionScene;
    PhysicsScene2D m_predictionPhysicsScene;

    private Action<List<Vector2>>  m_predictCallbak;
    
    List<GameObject> m_dummyObstacles = new List<GameObject>();

    [SerializeField] private ProjectionBall m_projectionBall;
    
    public void CreatePhysicsScene(Transform baseObsticles, int iterations)
    {
        CreateSceneParameters parameters = new CreateSceneParameters(LocalPhysicsMode.Physics2D);
        m_predictionScene = SceneManager.CreateScene("Prediction", parameters);
        m_predictionPhysicsScene = m_predictionScene.GetPhysicsScene2D();
        m_maxIterations = iterations;
        copyAllObstacles(baseObsticles,true);
    }

    public void copyAllObstacles(Transform sourceTransform, bool neverDestroy=false){
        foreach(Transform t in sourceTransform)
        {
         
            if(t.gameObject.GetComponent<Collider2D>() != null)
            {
                GameObject fakeT = Instantiate(t.gameObject);
            
                Destroy(fakeT.GetComponent<SpriteRenderer>());
                Destroy(fakeT.GetComponent<CollectibleView>());
                Destroy(fakeT.GetComponent<ObsticleView>());

                fakeT.transform.position = t.position;
                fakeT.transform.rotation = t.rotation;
                fakeT.transform.localScale = t.localScale;
                
                SceneManager.MoveGameObjectToScene(fakeT, m_predictionScene);
                
                //allows me to add the side colliders once
                if(!neverDestroy)
                    m_dummyObstacles.Add(fakeT);
            }
        }
    }

   
    public void killAllObstacles(){
        foreach(var o in m_dummyObstacles){
            Destroy(o);
        }
        m_dummyObstacles.Clear();
    }

    public void SetPredictionCallBack(Action<List<Vector2>> preditionCallback)
    {
        m_predictCallbak = preditionCallback;
    }
    
    public void Predict(float scale, Vector2 currentPosition, Vector2 force){
        if (m_predictionPhysicsScene.IsValid())
        {
            var dummy = Instantiate(m_projectionBall);
            SceneManager.MoveGameObjectToScene(dummy.gameObject, m_predictionScene);
 
            dummy.Init(scale,currentPosition,force);
            List<Vector2> points = new List<Vector2>();
            for (int i = 0; i < m_maxIterations; i++)
            {
                dummy.Colliding = false;
                m_predictionPhysicsScene.Simulate(Time.fixedDeltaTime);
                
                if (dummy.Colliding)
                    points.Add(dummy.transform.position);
                
            }

            m_predictCallbak(points);
            Destroy(dummy.gameObject);
        }
       
    }

    void OnDestroy(){
        killAllObstacles();
    }


}
