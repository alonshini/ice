﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public class SoundsController : MonoBehaviour
{

    [SerializeField] AudioClip m_aiming = null;//DONE
    [SerializeField] List<AudioClip> m_ballHitTarget = null;//DONE

    [SerializeField] AudioClip m_button = null;

    
    [SerializeField] AudioClip m_shoting;//DONE
    [SerializeField] AudioClip m_stageComplete;//DONE
    [SerializeField] AudioClip m_stageCompleteStars;//DONE
    [SerializeField] AudioClip m_levelFailed;

    [SerializeField] AudioSource m_SFX_Source1 = null;
    [SerializeField] AudioSource m_SFX_Source2 = null;
    [SerializeField] AudioSource m_SFX_Source3 = null;
    [SerializeField] AudioSource m_SFX_Source4 = null;
    [SerializeField] AudioSource m_SFX_Source5 = null;

    [SerializeField] AudioSource m_aimSource = null;
    
    static SoundsController m_instance;

    public static SoundsController Instance
    {
        get
        {

            if (m_instance == null)
            {
                m_instance = GameObject.FindObjectOfType<SoundsController>();

            }
            return m_instance;
        }
    }

    public void PlayButtonClicked()
    {
        PlayClip(m_button);
    }


    public void DisableEnableMixer(bool disable)
    {
        if (disable)
            AudioListener.volume = 0;
        else
            AudioListener.volume = 1f;

    }

    public void PlayAim(bool start)
    {
        if (start)
        {
            m_aimSource.clip = m_aiming;
            m_aimSource.Play();
        }
        else
        {
            m_aimSource.Stop();
        }
    }


    public void PlayBallHit()
    {
        PlayClip(m_ballHitTarget[0]);
    }




    public void PlayShotBall()
    {
        PlayClip(m_shoting);
    }


    public void PlaySummaryScreen(bool stars)
    {
        if (stars)
            PlayClip(m_stageCompleteStars);
        else
            PlayClip(m_stageComplete);
    }

    public void PlayLevelFailed()
    {
        PlayClip(m_levelFailed);
    }

    public void CleanAllSFXChannels()
    {
        m_SFX_Source1.clip = null;
        m_SFX_Source2.clip = null;
        m_SFX_Source3.clip = null;
        m_SFX_Source4.clip = null;
        m_SFX_Source5.clip = null;
    }

    public void StopClip(AudioSource audio_source)
    {
        if (audio_source != null)
        {
            audio_source.Stop();
            audio_source.clip = null;
        }
    }

    public AudioSource PlayClip(AudioClip clip, float volume = 1, float pitch = 1)
    {
        AudioSource audio_source = GetFreeAudioSource();

        if (audio_source != null && audio_source.enabled == true)
        {
            audio_source.clip = clip;
            audio_source.Play();
            audio_source.pitch = pitch;
            audio_source.volume = volume;
        }

        return audio_source;
    }



    private AudioSource GetFreeAudioSource()
    {
        if (!m_SFX_Source1.isPlaying)
            return m_SFX_Source1;


        if (!m_SFX_Source2.isPlaying)
            return m_SFX_Source2;

        if (!m_SFX_Source3.isPlaying)
            return m_SFX_Source3;

        if (!m_SFX_Source4.isPlaying)
            return m_SFX_Source4;

        if (!m_SFX_Source5.isPlaying)
            return m_SFX_Source5;


        return null;

    }
    public void ChannelOnOff(bool music_channel, bool channel_on)
    {

        if (!channel_on)
            StopAllSFX();

        m_SFX_Source1.enabled = channel_on;
        m_SFX_Source2.enabled = channel_on;
        m_SFX_Source3.enabled = channel_on;
        m_SFX_Source4.enabled = channel_on;
        m_SFX_Source5.enabled = channel_on;

    }

    public void StopAllSFX()
    {
        StopClip(m_SFX_Source1);
        StopClip(m_SFX_Source2);
        StopClip(m_SFX_Source3);
        StopClip(m_SFX_Source4);
        StopClip(m_SFX_Source5);

    }


    AudioClip PickRandomFromArray(AudioClip[] array)
    {
        if (array == null || array.Length == 0)
            return null;
        int pick = UnityEngine.Random.Range(0, array.Length);
        return array[pick];
    }


}
