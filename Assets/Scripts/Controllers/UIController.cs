using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class UIController : MonoBehaviour
{
    enum GameState
    {
        Init,
        Splash,
        Menu,
        Game
    }
    
    public static UIController Instance;

    [SerializeField] private GameObject m_splashScreen;
    [SerializeField] private GameObject m_menuScreen;
    [SerializeField] private GameObject m_gameScreen;
    [SerializeField] private InstructionsView m_instructions;
    [SerializeField] private LevelSummaryView m_levelSummary;
    [SerializeField] private DebugView m_debug;
    
    private GameState m_gameState = GameState.Init;
    private bool m_dragging = false;
    private Vector3 m_prevMousePos;

   

    private void Awake()
    {
        Instance = this;
        
        m_splashScreen.SetActive(false);
        m_menuScreen.SetActive(false);
        m_gameScreen.SetActive(false);
        
    }

    public void InitUI()
    {
        m_splashScreen.SetActive(true);
        m_menuScreen.SetActive(false);
    }
    
    public void PointerDown()
    {
        
    }
    
    public void StartDrag()
    {
        m_dragging = true;
        m_prevMousePos = Input.mousePosition;
    }
 
    public void Dragging()
    {
        if(m_dragging)
            if (m_gameState == GameState.Game)
            {
                Vector3 deltaMovement =(Input.mousePosition - m_prevMousePos)/Time.deltaTime;
                GameController.Instance.UIDragging(deltaMovement);
                m_prevMousePos = Input.mousePosition;
                
                //Debug.Log("Delta: " + deltaMovement);
            }
    }

    public void EndDrag()
    {
        m_dragging = false;
    }

    public void PointerUp()
    {
        if (m_gameState == GameState.Game && m_dragging==false)
            GameController.Instance.UIPointerUPDown(false);
        else if (m_gameState == GameState.Init)
            SplashClicked();
        else if (m_gameState == GameState.Menu)
            MenuClicked();
    }

    private void SplashClicked()
    {
        //show Menu Hide Splash
        m_splashScreen.SetActive(false);
        m_menuScreen.SetActive(true);
        m_gameState = GameState.Menu;
    }

    private void MenuClicked()
    {
        //menu clicked
        m_menuScreen.SetActive(false);
        m_gameScreen.SetActive(true);
        m_gameState = GameState.Game;
        GameController.Instance.LoadLevel(0);
    }
    
    public void ShowInstructions(string text = null, float autoHide = 0, Action ShowDone=null)
    {
        m_instructions.ShowInstructions(text,autoHide,ShowDone);
    }

    public void HideInstructions()
    {
        m_instructions.HideInstructions();
    }

    public IEnumerator StartLevelCompleteFlow(bool success, Action hideIce)
    {
        m_levelSummary.ShowLevelSummary(success);

        yield return new WaitForSeconds(1.5f);
        hideIce?.Invoke();
    }

    public void ResetLevelComplete()
    {
        m_levelSummary.HideLevelSummary();
    }
    
    public void HideLevelSummary()
    {
        m_levelSummary.HideLevelSummary();
    }

    public void LevelUpDownClicked(bool up)
    {
        GameController.Instance.MoveAndLoadLevelUpDown(up);
    }

    public void PeekClicked()
    {
        GameController.Instance.ShowUnderIce();
    }

    public void SetLevelIndex(int levelIndex)
    {
        m_debug.UpdateLevelIndex("LEVEL\n #"+levelIndex);
    }
}
