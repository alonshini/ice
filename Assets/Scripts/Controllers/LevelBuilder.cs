using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBuilder : MonoBehaviour
{
    [SerializeField] private Transform m_collectibles;
    [SerializeField] private Transform m_obsticles;
    [SerializeField] private Transform m_levelsHolder;

    private LevelView m_currLevelView;
    List<CollectibleView> m_currCollectibles = new List<CollectibleView>();
    List<ObsticleView> m_currObsticles = new List<ObsticleView>();
    private GameObject m_currDispenser;
    private int m_numLevels = 0;
    public Transform Obsticles => m_obsticles;

    public int NumLevels => m_numLevels;

    public List<CollectibleView> CurrCollectibles => m_currCollectibles;

    public void InitBuilder()
    {
        foreach (Transform level in m_levelsHolder.transform)
        {
            level.gameObject.SetActive(false);
            m_numLevels = NumLevels + 1;
        }
    }
    
    public LevelView BuildLevel(int index)
    {
        foreach (Transform collectible in m_collectibles)
            Destroy(collectible.gameObject);
        
        foreach (Transform obsticle in m_obsticles)
            Destroy(obsticle.gameObject);

        Destroy(m_currDispenser);
        
        CurrCollectibles.Clear();
        m_currObsticles.Clear();
        m_currDispenser = null;
        int numLevelColletibles = 0;
        
        GameObject currLevel = Instantiate(m_levelsHolder.transform.GetChild(index).gameObject, transform);
        m_currLevelView = currLevel.GetComponent<LevelView>();
        
        foreach (Transform child in m_currLevelView.Holder)
        {
            //check if the child is item or platform or dispenser

            if (child.GetComponent<ObsticleView>() != null)
                m_currObsticles.Add(child.GetComponent<ObsticleView>());
            else if (child.GetComponent<CollectibleView>() != null)
                CurrCollectibles.Add(child.GetComponent<CollectibleView>());
            else if (child.name == "Dispenser")
                m_currDispenser = child.gameObject;
            
        }

        foreach (ObsticleView obsticle in m_currObsticles)
            obsticle.transform.SetParent(Obsticles);

        foreach (CollectibleView collectible in CurrCollectibles)
        {
            collectible.Init(m_collectibles);
            
            if (collectible.m_collectibleType == CollectibleView.CollectibleType.Fish)
                numLevelColletibles++;
            
            
        }
        
        if(m_currDispenser!=null)
            m_currDispenser.transform.SetParent(Obsticles);
        else
            Debug.Log("Level: " + index + "Has no dispenser");


        m_currLevelView.NumCollectibles = numLevelColletibles;
        m_currLevelView.SetDispenserObject(m_currDispenser);
        
        Destroy(currLevel.gameObject);
        
        return m_currLevelView;
    }
    
}
