using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;


public class TextureController : MonoBehaviour
{
    [SerializeField] SpriteRenderer m_overBGImage;
    [SerializeField] Texture2D m_orgTexture;

    private Bounds m_bounds = new Bounds(new Vector3(), new Vector3(5.62f, 10));
    private float m_boundsExtentX;
    private float m_boundsExtentY;
    private int m_totalPixels;
    private Color m_clearColor = new Color(1, 1, 1, 0);
    private int m_clearedCounter = 0;
    private Texture2D m_currTexture;
    private int m_textureWidthPixels, m_textureHeightPixels;
    private Color32[] m_tempArray;
    private Color32[] m_currArray;
    private Color32[] m_tempCircArray;
    private List<Vector2Int> m_circTempMinMaxList;
    private int m_arrSize;
    

    public void InitParams(Bounds playArea)
    {
        m_bounds = playArea;
        m_boundsExtentX = m_bounds.extents.x;
        m_boundsExtentY = m_bounds.extents.y;
        m_textureWidthPixels = 366;
        m_textureHeightPixels = 512;
        m_totalPixels = m_textureWidthPixels * m_textureHeightPixels;
        
    }

    public Transform BuildIceCover()
    {
        m_currTexture = new Texture2D(m_textureWidthPixels, m_textureHeightPixels);
        m_currTexture.SetPixels(m_orgTexture.GetPixels());
        m_currTexture.Apply();
        m_currArray = m_orgTexture.GetPixels32();

        m_overBGImage.sprite = Sprite.Create(m_currTexture, new Rect(0, 0, m_textureWidthPixels, m_textureHeightPixels),
            Vector2.zero);
        m_overBGImage.material.mainTexture = m_currTexture;
        return m_overBGImage.transform;
    }

    public void HideIceCover()
    {
        m_overBGImage.gameObject.SetActive(false);
    }
    
    private void ClearRectTriShapeFromTexture(Vector2 startPosition, Vector2 endPosition, Vector2 perp)
    {
        //Debug.Log("Start Clearing Rect:" + startPosition +" " + endPosition);
      
        float prepWidth = 1f;
        prepWidth = GameController.Instance.GetBallSize();

        float perpSize = perp.magnitude;
        
        perp = perp.normalized;
       
        m_tempArray = m_orgTexture.GetPixels32();

        List<Vector2Int> minMaxList = new List<Vector2Int>();

        Vector2 m_startMinPerp, m_startMaxPerp, m_endMinPerp, m_endMaxPerp,midMinPerp,midMaxPerp;
        
        Vector2 center = new Vector2(startPosition.x - endPosition.x, startPosition.y - endPosition.y);

        m_startMinPerp = new Vector2(startPosition.x + (perp.x * -prepWidth), startPosition.y + (perp.y * -prepWidth));
        m_startMaxPerp = new Vector2(startPosition.x + (perp.x * prepWidth), startPosition.y + (perp.y * prepWidth));

        m_endMinPerp = new Vector2(endPosition.x + (perp.x * -prepWidth), endPosition.y + (perp.y * -prepWidth));
        m_endMaxPerp = new Vector2(endPosition.x + (perp.x * prepWidth), endPosition.y + (perp.y * prepWidth));

        // adding 2 more points to create a triangle shape
        //points are after the size of the rect - and in the middle
        midMinPerp = new Vector2(startPosition.x + (perp.x * prepWidth * -Random.Range(1f,2f)),
            startPosition.y + (perp.y * prepWidth*-2));
        midMaxPerp = new Vector2(startPosition.x + (perp.x * prepWidth * Random.Range(1f,2f)),
            startPosition.y + (perp.y * prepWidth*2));
        
        midMinPerp = midMinPerp - Vector2.Perpendicular(perp)*UnityEngine.Random.value*perpSize;
        midMaxPerp = midMaxPerp - Vector2.Perpendicular(perp)*UnityEngine.Random.value*perpSize;
        
        BuildWorldLineToTexture(m_startMinPerp,m_startMaxPerp,minMaxList);
        BuildWorldLineToTexture(m_endMinPerp,m_endMaxPerp,minMaxList);
        BuildWorldLineToTexture(m_startMaxPerp,midMaxPerp,minMaxList);
        BuildWorldLineToTexture(m_endMaxPerp,midMaxPerp,minMaxList);
        BuildWorldLineToTexture(m_startMinPerp,midMinPerp,minMaxList);
        BuildWorldLineToTexture(m_endMinPerp,midMinPerp,minMaxList);
        
        //get shape center 
        Vector2 startFillPosition = new Vector2(startPosition.x - center.x / 2, startPosition.y - center.y / 2);
        Vector2 tempStartFill = GetPixelPosition(startFillPosition, m_textureWidthPixels, m_textureHeightPixels);
        Vector2Int startFill = new Vector2Int(Convert.ToInt32(tempStartFill.x), Convert.ToInt32(tempStartFill.y));
        
        FloodFill(startFill);

        //find min and max x y to run only in that range
        Vector2Int startEndIndex = GetMinMaxIndexes(minMaxList);
        //union both temp and curr arr 
        CopyTempToCurr(m_tempArray, startEndIndex.x, startEndIndex.y);

        m_currTexture.SetPixels32(m_currArray);
        m_currTexture.Apply();

        //Debug.Log("End Clearing Rect:" + startPosition +" " + endPosition);
    }

    public void ClearExplosionFromTexture(Vector2 center, float bombRadius)
    {
        Vector2 centerPixelPosition = GetPixelPosition(center, m_textureWidthPixels, m_textureHeightPixels);

        int remain = 360;
        float radius = bombRadius;
        
        List<int> angles = new List<int>();
        List<Vector2> points = new List<Vector2>();
        
        m_tempArray = m_orgTexture.GetPixels32();

        List<Vector2Int> minMaxList = new List<Vector2Int>();
        
        angles.Add(0);
        
        
        while (remain>0)
        {
            int nextPoint = UnityEngine.Random.Range(25, 65);
            
            angles.Add(nextPoint+360-remain);

            remain -= nextPoint;
        }

        if(angles[angles.Count-1]>=360)
            angles.RemoveAt(angles.Count-1);

        foreach (int angle in angles)
        {
            float randomRadius = UnityEngine.Random.value * radius*1.5f;
            float currRadius = radius/2f + randomRadius;
            
            float x = center.x + Mathf.Cos(angle*Mathf.Deg2Rad)*currRadius;
            float y = center.y + Mathf.Sin(angle*Mathf.Deg2Rad)*currRadius;
            
            points.Add(new Vector2(x,y));
        }

        for (int i = 0; i < points.Count-1; i++)
            BuildWorldLineToTexture(points[i],points[i+1],minMaxList);
        
        BuildWorldLineToTexture(points[points.Count-1],points[0],minMaxList);
        
        
        Vector2Int startFill = new Vector2Int(Convert.ToInt32(centerPixelPosition.x), Convert.ToInt32(centerPixelPosition.y));
        
        FloodFill(startFill);

        //find min and max x y to run only in that range
        Vector2Int startEndIndex = GetMinMaxIndexes(minMaxList);
        //union both temp and curr arr 
        CopyTempToCurr(m_tempArray, startEndIndex.x, startEndIndex.y);

        m_currTexture.SetPixels32(m_currArray);
        m_currTexture.Apply();
        
    }
    
    private void CopyTempToCurr(Color32[] tempArray, int startIndex, int endIndex)
    {
        for (int i = startIndex; i < endIndex; i++)
        {
            if (tempArray[i] == m_clearColor)
            {
                if (m_currArray[i] != m_clearColor)
                    m_clearedCounter++;

                m_currArray[i] = m_clearColor;
            }
        }
    }

    public void ClearRect(Vector3 startPosition, Vector3 endPosition, Vector2 perp)
    {
        ClearRectTriShapeFromTexture((Vector2) startPosition, endPosition, perp);
    }


    /// <summary>
    /// gets start and end vectors for the line - and a list to add all the points into
    /// gets the direction and distance between the points
    /// builds the outline
    /// minMaxList is used to later more efficiently run over the texture pixels that needs to be modified
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="minMaxList"></param>
    private void BuildWorldLineToTexture(Vector2 start, Vector2 end, List<Vector2Int> minMaxList)
    {
        //convert world space points to pixel positions in a texture
        Vector2 startPix = GetPixelPosition(start, m_textureWidthPixels, m_textureHeightPixels);
        Vector2 endPix = GetPixelPosition(end, m_textureWidthPixels, m_textureHeightPixels);
        //gets the direction between the 2 points
        Vector2 direction = new Vector2(endPix.x - startPix.x, endPix.y - startPix.y);
        //gets the direction lenghts to know how many pixels it needs to have
        int directionLen = Convert.ToInt32(direction.magnitude) + 1;
        direction.Normalize();
        List<Vector2> outline = new List<Vector2>();
        
        //adds to outline array all the points along the vector of the outline
        for (int i = 0; i < directionLen; i++)
            outline.Add(new Vector2(startPix.x + direction.x * i,
                startPix.y + direction.y * i));
        
        //convert outline points to pixels - mark them in the array - add them to min max list
        for (int i = 0; i < outline.Count; i++)
        {
            int cx = Convert.ToInt32(outline[i].x);
            int cy = Convert.ToInt32(outline[i].y);
            int index = cy * m_textureWidthPixels + cx;

            //check point isn't out of bounds
            if (index>=0 && index < m_tempArray.Length)
            {
                m_tempArray[index] = m_clearColor;
                minMaxList.Add(new Vector2Int(cx, cy));
            }
        }
        
    }
    
    private void FloodFill(Vector2Int pos)
    {
        Queue<Vector2Int> queue = new Queue<Vector2Int>();

        Fill(pos, queue);

        while (queue.Any())
        {
            Vector2Int toCheck = queue.Dequeue();

            Fill(toCheck + new Vector2Int(-1, 0), queue);
            Fill(toCheck + new Vector2Int(0, 1), queue);
            Fill(toCheck + new Vector2Int(1, 0), queue);
            Fill(toCheck + new Vector2Int(0, -1), queue);
        }
    }

    private void Fill(Vector2Int pos, Queue<Vector2Int> queue)
    {
        if ((pos.x < 0) || (pos.x >= m_textureWidthPixels)) return;
        if ((pos.y < 0) || (pos.y >= m_textureHeightPixels)) return;
        if (m_tempArray[pos.x + pos.y * m_textureWidthPixels] != m_clearColor)
        {
            m_tempArray[pos.x + pos.y * m_textureWidthPixels] = m_clearColor;
            queue.Enqueue(pos);
        }
    }

    /// <summary>
    /// gets a list of min and max values and calculates the start and end indexes matching
    /// those mind and max using the default image width
    /// </summary>
    /// <param name="minMaxList"></param>
    /// <returns></returns>
    private Vector2Int GetMinMaxIndexes(List<Vector2Int> minMaxList)
    {
        int minX = minMaxList[0].x, maxX = minMaxList[0].x, minY = minMaxList[0].y, maxY = minMaxList[0].y;

        for (int i = 0; i < minMaxList.Count; i++)
        {
            if (minMaxList[i].x > maxX)
                maxX = minMaxList[i].x;
            if (minMaxList[i].x < minX)
                minX = minMaxList[i].x;
            if (minMaxList[i].y > maxY)
                maxY = minMaxList[i].y;
            if (minMaxList[i].y < minY)
                minY = minMaxList[i].y;
        }

        int startIndex = minY * m_textureWidthPixels + minX;
        int endIndex = maxY * m_textureWidthPixels + maxX;

        return new Vector2Int(startIndex, endIndex);
    }

    private Vector2 GetPixelPosition(Vector2 pos, int imageWidth, int imageHeight)
    {
        float newX = MapToFloat(m_boundsExtentX, pos.x) * imageWidth;
        float newY = MapToFloat(m_boundsExtentY, pos.y) * imageHeight;

        return new Vector2(newX, newY);
    }

    private float MapToFloat(float range, float value)
    {
        return (value + range) / (range + range);
    }

    public void FadeIceOutIn(float duration)
    {
        m_overBGImage.DOFade(0, 0.25f);
        m_overBGImage.DOFade(1, 0.25f).SetDelay(duration+0.25f);
    }
}