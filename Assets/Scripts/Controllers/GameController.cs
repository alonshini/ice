using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class GameController : MonoBehaviour
{
    enum InGameState
    {
        Init,
        LoadingLevel,
        LevelStartClicked,
        Aiming,
        Flying,
        WaitForStop,
        ShotEnded,
        LevelEnded,
        LevelSummary
           
    }
    
    public static GameController Instance;
    
    [SerializeField] private CameraView m_cameraView;
    [SerializeField] private SpriteRenderer m_playArea;
    [SerializeField] private Transform m_staticSideColliders;
    
    [SerializeField] private TextureController m_textureController;
    [SerializeField] private LevelBuilder m_levelBuilder;
    [SerializeField] private DispenserManager m_dispenserManager;

    [SerializeField] private GameObject m_ballPrefab;
    [SerializeField] private Animator m_iceAnimator;
    
    private int m_currLevelIndex;
    private BallView m_currBallView;    
    private InGameState m_InGameState;
    private bool m_pointerDown=false;
    private int m_ballHits;
    private LevelView m_currLevelView;
    private int m_currShotsLeft;
    private int m_numCollectedCollectibles;
    private bool m_levelComplete;

    private bool m_report = true;
    private Coroutine m_showLevel;
    public int CurrLevelIndex
    {
        get => m_currLevelIndex;
        set => m_currLevelIndex = value;
    }

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {/*
        Vector2 temp = Vector2.up;
        
        
        Vector2 topLeft = new Vector2(-5, 5);
        Vector2 topRight = new Vector2(5, 5);
        Vector2 botLeft = new Vector2(-5, -5);
        Vector2 botRight = new Vector2(5, -5);
        
        Debug.Log(Vector2.SignedAngle(temp,topLeft));
        Debug.Log(Vector2.SignedAngle(temp,topRight));
        Debug.Log(Vector2.SignedAngle(temp,botLeft));
        Debug.Log(Vector2.SignedAngle(temp,botRight));
*/
       
        Init();
    }

    // Start is called before the first frame update
    private void Init()
    {
        m_cameraView.SetCamera(m_playArea.bounds.size.x);
        m_textureController.InitParams(m_playArea.bounds);
        m_dispenserManager.InitSimulation(m_staticSideColliders,450);
        m_levelBuilder.InitBuilder();
        UIController.Instance.InitUI();
    }

    public void LoadLevel(int index)
    {
        m_iceAnimator.SetTrigger("reset");
        
        if(m_showLevel!=null)
            StopCoroutine(m_showLevel);
        
        UIController.Instance.ResetLevelComplete();
        
        m_InGameState = InGameState.LoadingLevel;
        CurrLevelIndex = index;
        UIController.Instance.SetLevelIndex(m_currLevelIndex);
        m_currLevelView = m_levelBuilder.BuildLevel(index);
        m_dispenserManager.SetDispenserObject(m_currLevelView,m_levelBuilder);
        m_currShotsLeft = m_currLevelView.NumShots;
        m_levelComplete = false;
        m_numCollectedCollectibles = 0;
        m_showLevel = StartCoroutine(StartLevelFlow());
        
    }
 
    private void GenerateBall()
    {
        GameObject currBall = Instantiate(m_ballPrefab, m_dispenserManager.DispenserObject.transform);
        m_currBallView = currBall.GetComponent<BallView>();
        m_currBallView.SetMe(new Vector3(1f, 1f,1f));
    }
    
    IEnumerator StartLevelFlow()
    {
        //create and show Ball
        GenerateBall();
        
        //bring all the level collectibles
        for (int i = 0; i < m_levelBuilder.CurrCollectibles.Count; i++)
        {
            m_levelBuilder.CurrCollectibles[i].AppearInWater();
            yield return new WaitForSeconds(.45f);
        }
        
        //add aditional items to level if applicapble
        Debug.Log("Bringing additional items");
        
        UIController.Instance.ShowInstructions("Look for the targets\nTap to start");

        while (m_InGameState==InGameState.LoadingLevel)
        {
            yield return new WaitForEndOfFrame();
        }

        UIController.Instance.HideInstructions();
        
        //bring ice cover
        Transform iceCover = m_textureController.BuildIceCover();
        iceCover.gameObject.SetActive(true);
        
        m_iceAnimator.SetTrigger("showIce");
        yield return new WaitForSeconds(.5f);
        
        m_dispenserManager.ShowHidePrediction(true);
        UIController.Instance.ShowInstructions("Swipe To aim\nTap to shot");
        //enable aiming
        m_InGameState = InGameState.Aiming;

        m_showLevel = null;
    }

    private void ShotProjectile()
    {

        m_dispenserManager.ShowHidePrediction(false);
        UIController.Instance.HideInstructions();
        m_InGameState = InGameState.Flying;
        m_currShotsLeft--;
        m_ballHits = 0;
        m_currBallView.ShotMe(m_dispenserManager.CurrShot);

        //m_textureController.ClearExplosionFromTexture(Camera.main.ScreenToWorldPoint(Input.mousePosition));
    }
    
    
    
    public void UIPointerUPDown(bool pointerDown)
    {
        if (!pointerDown)
        {
            switch (m_InGameState)
            {
                case InGameState.Aiming:
                {
                    //shots the projectile
                    ShotProjectile();
                    break;
                }
                case InGameState.LoadingLevel:
                {
                    m_InGameState = InGameState.LevelStartClicked;
                    break;
                    
                }
                case InGameState.LevelSummary:
                {
                    LevelSummaryClicked();
                    break;
                    
                }
            }
        }
       
    }

    public void UIDragging(Vector2 delta)
    {
        if (m_InGameState == InGameState.Aiming)
        {
            //add the Y component depending on angle 
            m_dispenserManager.AdjustAngle(delta);
        }
        
    }

    public void ReportBallPosition(Vector3 startPosition,Vector3 endPosition,Vector2 perp)
    {
       
        if (m_report)
        {
            m_textureController.ClearRect((Vector2) startPosition,endPosition,perp);
            //m_report = false;
        }
    }

    public void BallHitCollider(Vector3 hitPosition)
    {
        //need to check if already reach max hits - and if so - kill the ball
        m_ballHits++;

       // if (m_ballHits > m_currLevelView.NumprojectionShots)
      //  {
            m_InGameState = InGameState.WaitForStop;
            //m_currBallView.SlowDown();
      //  }
    }

    public bool CheckForStop()
    {
        return m_InGameState == InGameState.WaitForStop;
    }
    
    public void BallStopped(Vector3 stopPosition)
    {
        Destroy(m_currBallView.gameObject);
            
        //check level complete
        m_levelComplete = m_numCollectedCollectibles==m_currLevelView.NumCollectibles;

        //check balls finished or complete
        if (m_currShotsLeft <= 0 || m_levelComplete)
        {
            m_InGameState = InGameState.LevelSummary;
            
            //show round summary
            StartCoroutine(
                UIController.Instance.StartLevelCompleteFlow(m_levelComplete, m_textureController.HideIceCover));
            
        }
        else
        {
            //makes the game resume from end position of last shot
            m_dispenserManager.ResetDispenserObjectPosition(stopPosition);
            m_dispenserManager.ShowHidePrediction(true);
            GenerateBall();
            m_InGameState = InGameState.Aiming;
        }
    }
    
    public void BallHitTrigger(Collider2D other)
    {
        CollectibleView currCollectibleView = other.gameObject.GetComponentInParent<CollectibleView>();

        currCollectibleView.PlayCollideAnimation();

        if (currCollectibleView.m_collectibleType == CollectibleView.CollectibleType.Bomb)
        {
            m_textureController.ClearExplosionFromTexture(currCollectibleView.transform.position,
                (currCollectibleView as BombCollectibleView).m_bombSize);
            //change the ball impact - to reflect bomb effect
            m_currBallView.HitByBomb();

            if (m_InGameState == InGameState.WaitForStop)
                m_InGameState = InGameState.Flying;
        }
        else
            m_numCollectedCollectibles++;

    }

    private void LevelSummaryClicked()
    {
        m_InGameState = InGameState.LevelEnded;
        UIController.Instance.HideLevelSummary();
        
        //check if level was complete - load next one - else load same again

        if (m_levelComplete)
        {
            CurrLevelIndex++;
            CurrLevelIndex = CurrLevelIndex % m_levelBuilder.NumLevels;
        }
        
        LoadLevel(CurrLevelIndex);
    }

    public void MoveAndLoadLevelUpDown(bool up)
    {
        CurrLevelIndex += up ? 1 : -1;
        CurrLevelIndex = (CurrLevelIndex+m_levelBuilder.NumLevels) % m_levelBuilder.NumLevels;
        LoadLevel(CurrLevelIndex);
    }

    public void ShowUnderIce()
    {
        m_textureController.FadeIceOutIn(2);
    }
    
    public float GetBallSize()
    {
        return m_currLevelView.BallSize;
    }

    public int GetCurrLevelForceMulti()
    {
        return m_currLevelView.LevelForceMulti;
    }
}
