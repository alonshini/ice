using System;
using UnityEngine;
using UnityEngine.Animations;

public class Orbitor : MonoBehaviour 
{
    public Vector4 m_radius;
    private float m_speed = 15f;
    private Vector3 m_axis = Vector3.up;
    public Vector3 m_center;
    private void Start()
    {
        m_speed = m_radius.w;
        
        if(m_radius.y>0)
            m_axis = Vector3.left;
        
        m_center = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        
        transform.position  += (Vector3)m_radius;

    }

    private void Update() {
        transform.RotateAround (m_center, m_axis, m_speed * Time.deltaTime);
    }
}