using UnityEngine;

public class Rotator : MonoBehaviour 
{
    public Vector3 _rot;


    private void Update() {
        transform.Rotate(_rot * Time.deltaTime);
    }
}