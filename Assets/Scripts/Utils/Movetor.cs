    using System;
    using UnityEngine;
    using UnityEngine.Animations;

    public class Movetor : MonoBehaviour 
    {
        
          public Vector4 m_radius;
          private float m_speed;

          private bool m_changeY = false;
          private bool m_changeX = false;
          private bool m_changeZ = false;

          private float m_orgY;
          private float m_orgX;
          private float m_orgZ;
          
          private float m_maxY;
          private float m_maxX;
          private float m_maxZ;
          private void Start()
          {
              m_speed = m_radius.w;

              if (m_radius.x != 0)
              {
                  m_changeX = true;
                  m_maxX = m_radius.x;
              }
              
              m_orgX = transform.position.x;

              if (m_radius.z != 0)
              {
                  m_changeZ = true;
                  m_maxZ = m_radius.z;
              }
              
              m_orgZ = transform.position.z;

              if (m_radius.y != 0)
              {
                  m_changeY = true;
                  m_maxY = m_radius.y;
              }
              
              m_orgY = transform.position.y;

          }


          public void Update()
          {
              float y = m_orgY;
              float x = m_orgX;
              float z = m_orgZ;
              
              if(m_changeY)
                y = Mathf.PingPong(Time.time * m_speed, 1) * m_maxY*2 -m_maxY +m_orgY;
              
              if(m_changeX)
                  x = Mathf.PingPong(Time.time * m_speed, 1) * m_maxX+m_orgX;
              
              if(m_changeZ)
                  z = Mathf.PingPong(Time.time * m_speed, 1) * m_maxZ*2 - m_maxZ+m_orgZ;
              
              transform.position = new Vector3(x, y, z);
          }
    }